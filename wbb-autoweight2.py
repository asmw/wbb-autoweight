#!/usr/bin/env python3

# Released by asmw under the Unlicense (unlicense.org)
# Based on https://gist.github.com/rdb/8864666

"""Wii Balance Board Automatic Scale v2.0

Usage: wbb-autoweight2.py [-n] [-c COUNT] [-j JSDEV] [-b address] [-l LIMIT] [-d DISCARD] [-u USER] [-a]

-n --nowait                     do not wait for the joystick device
-c COUNT --count=COUNT          limit number of samples to COUNT. COUNT < 0 means no limit [default: 1500]
-j JSDEV --jsdev=JSDEV          the joystick input device to use [default: /dev/input/js0]
-b ADDRESS --btaddress=ADDRESS  the address of the Wii Balance Board to disconnect after usage
-l LIMIT --limit=LIMIT          the lower limit for samples, assumes kg if < 100, g otherwise [default: 40]
-d DISCARD --discard=DISCARD    discard the first DISCARD samples [default: 500]
-u USER --user USER             use the user USER instead of guessing
-a --all                        save all datapoints for a measurement
"""

from docopt import docopt
import wbb
import os
import time
import json
import sys
import statistics

data = {}
user = None
data_file = "data2.json"

def load_data(data_file):
    if os.path.exists(data_file):
        with open(data_file) as data_file:
            data = json.load(data_file)
            return data
    else:
        if user == None:
            print("No data file found, please specify an initial user via -u/--user")
            sys.exit(2)
        else:
            data = {}
            data[user] = {}
            data[user]['data'] = []
            return data

def save_data(data, weight, samples, out_file):
    if not user in data:
        data[user] = {}
        data[user]['data'] = []
    data[user]['last'] = weight
    data[user]['data'].append([time.time(), weight, samples])
    data['last_user'] = user
    with open(out_file, 'w') as data_file:
        json.dump(data, data_file)

def find_user(data, weight):
    current = None
    closest = 10000.0
    for u in data:
        if u == 'last_user':
            continue
        cur = abs(data[u]['last'] - weight)
        if cur < closest:
            closest = cur
            current = u
    print(f"Guessing user: {current} with offset {closest}")
    return current

if __name__ == '__main__':
    arguments = docopt(__doc__, version='WiiBalanceBoardAutoScale v2.0')
    js_file = arguments['--jsdev']
    btaddress = arguments['--btaddress']
    count = int(arguments['--count'])
    limit = int(arguments['--limit'])
    discard = int(arguments['--discard'])
    save_all = arguments['--all']
    user = arguments['--user']
    data = load_data(data_file)
    if not arguments['--nowait']:
        wbb.wait(js_file)
    jsdev = wbb.setup(js_file)
    print("Measuring...")
    samples = wbb.sample(jsdev, count, limit, False)
    if len(samples) > 0:
        res = statistics.mean(samples[-discard:])
        print(f"Result: {res/100.0}kg")
        if user == None:
            user = find_user(data, res)
        if user != None:
            save_data(data, res, samples if save_all else None , data_file)
        else:
            print("No user found")
    else:
        print("No samples")
    if btaddress:
        wbb.disconnect(btaddress)

