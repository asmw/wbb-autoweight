#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

WBB_ADDRESS=$(bt-device -l | grep "Nintendo" | sed -e "s/.*(\(.*\)).*/\1/")
POSTPROCESS_DIR=$DIR/postprocess/

if [ -z "$WBB_ADDRESS" ]; then
  echo "No balance board seems to be paired."
  exit 1
fi

cd $DIR
while true; do
    ./wbb-autoweight2.py -b $WBB_ADDRESS

    for pp_script in $POSTPROCESS_DIR/*; do
        if [ -x "$pp_script" ]; then
            echo "Running postprocess script: $pp_script"
            $pp_script $DIR/data2.json
        fi
    done

    sleep 1s
done
