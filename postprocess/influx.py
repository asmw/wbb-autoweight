#!/usr/bin/env python3
import os, sys, json
import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS

bucket = os.environ.get("INFLUXDB_BUCKET")
org = os.environ.get("INFLUXDB_ORG")
token = os.environ.get("INFLUXDB_TOKEN")
url = os.environ.get("INFLUXDB_URL")

client = influxdb_client.InfluxDBClient(
   url=url,
   token=token,
   org=org
)

write_api = client.write_api(write_options=SYNCHRONOUS)

last_user = None
weight = None

with open(sys.argv[1]) as f:
    data = json.load(f)
    last_user = data['last_user']
    weight = data[last_user]['last']/100.0

if not weight:
    print('No weight found')
    sys.exit(1)

p = influxdb_client.Point("weight").tag("user", last_user).field("weight", weight)
write_api.write(bucket=bucket, org=org, record=p)
