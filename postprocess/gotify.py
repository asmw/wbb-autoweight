#!/usr/bin/env python3
import requests
import os
import json
import sys

# Set the environment variables GOTIFY_HOST and GOTIFY_TOKEN before enabling this postprocess script

GOTIFY_HOST = os.environ.get("GOTIFY_HOST")
GOTIFY_TOKEN = os.environ.get("GOTIFY_TOKEN")
GOTIFY_FILTER_USERS = os.environ.get("GOTIFY_TOKEN")
PRIORITY = 2
weight = None
last_user = None

if not GOTIFY_HOST.startswith('http'):
    GOTIFY_HOST=f'https://{GOTIFY_HOST}'

with open(sys.argv[1]) as f:
    data = json.load(f)
    last_user = data['last_user']
    weight = data[last_user]['last']/100.0

if not weight:
    print('No weight found')
    sys.exit(1)

resp = requests.post(f'{GOTIFY_HOST}/message?token={GOTIFY_TOKEN}', json={
    "message": f"Last weight for {last_user}: {weight:.2f}",
    "priority": PRIORITY,
    "title": "Weight update"
})