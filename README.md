About
=====
wbb-autoweight allows you take regular weight measurements using a bluetooth enabled linux host and a Wii balance board.

The results are saved to a simple python file and can then be post-processed.

Requirements
============
  - A bluetooth adapter and a Wii balance board
  - A Linux kernel with the `hid_wiimote` driver supporting the balance board. This should be > 3.7, but for now only tested with 4.1/4.2.
  - `bluetoothctl` from the bluez package. This was introduced with bluez 5. Earlier version might work but I didn't test on hosts with bluez < 5.23.
  - The `bluez-tools` package for `bt-device`: `sudo apt install bluez-tools`
  - python and docopt
   - `sudo apt-get install python3-docopt`
   - or `pip install docopt`
  - Make sure the user has access to the bluetooth dbus service

Setup
=====
  - Pair the balance board with the host
    - `bluetoothctl`
    - `power on`
    - `scan on`
    - Press the red button in the balance boards battery compartment
    - wait until the scan results for your board appear (`[NEW] Device 00:11:22:33:44:55 Nintendo RVL-WBC-01`)
    - `pair 00:11:22:33:44:55`
    - `connect 00:11:22:33:44:55`
    - `trust 00:11:22:33:44:55`
    - `disconnect 00:11:22:33:44:55`

Usage
=====
```
./wbb-autoweight2.py -b $(bt-device -l | grep "Nintendo" | sed -e "s/.*(\(.*\)).*/\1/")
```

The included `autorun.sh` will do this for you and run `wbb-autoweight2.py` in an infinite loop so you can just kick the connect button on the balance board (wait for the blue light to stay on, sometimes it takes several presses), step on and step off when the light turns off again.

I leave it running on a Raspberry Pi in a tmux session.
