#!/usr/bin/env python3

# Released by asmw under the Unlicense (unlicense.org)
# Based on https://gist.github.com/rdb/8864666

"""Wii-Scale Client based on wbb.py

Usage: wbb-wii-scale.py [-h HOST] [-p PORT] [-b btaddress] [-j JSDEV]

-h HOST --host=HOST                 the host to connect to [default: localhost]
-p PORT --port=port                 the port to use [default: 8080]
-b BTADDRESS --btaddress=BTADDRESS  the Wii Balance Board to disconnect
-j JSDEV --jsdev=JSDEV              the joystick device to use [default: /dev/input/js0]
"""

from docopt import docopt
import os, struct, array, sys, subprocess, time, json
from fcntl import ioctl
import wbb
from socketIO_client import SocketIO, LoggingNamespace
import statistics

socket = None

if __name__ == '__main__':
    arguments = docopt(__doc__, version='Wii-Scale Client v0.1')
    host = arguments['--host']
    port = int(arguments['--port'])
    btaddress = arguments['--btaddress']
    js_file = arguments['--jsdev']
    print("Connecting")
    socket = SocketIO(host, port, LoggingNamespace)
    socket.emit('wiiscale-status', {'status': 'CONNECTING'})
    wbb.wait(js_file)
    jsdev = wbb.setup(js_file)
    socket.emit('wiiscale-status', {'status': 'CONNECTED'})
    socket.emit('wiiscale-status', {'status': 'READY'})
    socket.emit('wiiscale-status', {'status': 'MEASURING'})
    samples = wbb.sample(jsdev, 1500, 3000, False)
    total = statistics.mean(samples[-500:])/100.0
    socket.emit('wiiscale-weight', {'totalWeight': total})
    if btaddress:
        disconnect(btaddress)
    socket.emit('wiiscale-status', {'status': 'DONE'})
    socket.close()

