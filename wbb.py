#!/usr/bin/env python3

# Released by asmw under the Unlicense (unlicense.org)
# Based on https://gist.github.com/rdb/8864666

"""Wii Balance Board CLI tool

Usage: wbb-cli.py [-w] [-c COUNT] [-j JSDEV] [-o FILE] [-b address] [-l LIMIT] [-d DISCARD] [-s]

-w --wait                       wait for the joystick device
-c COUNT --count=COUNT          limit number of samples to COUNT. COUNT < 0 means no limit [default: -1]
-j JSDEV --jsdev=JSDEV          the joystick input device to use [default: /dev/input/js0]
-o FILE --out=FILE              write data to a file instead of stdout
-b ADDRESS --btaddress=ADDRESS  the address of the Wii Balance Board to disconnect after usage
-l LIMIT --limit=LIMIT          the lower limit for samples, assumes kg if < 100, g otherwise [default: 0]
-d DISCARD --discard=DISCARD    discard the first DISCARD samples [default: 0]
-s --summary                    print the average measured weight only
"""

from docopt import docopt
import os, struct, array, sys, subprocess, time, json
import statistics
from fcntl import ioctl

def wait(js_file):
    print('Waiting for balance board at %s' % js_file)
    while not os.path.exists(js_file):
        time.sleep(0.5)

def setup(js_file):
    # Open the joystick device.
    if not os.path.exists(js_file):
        print("No device to read from at: %s" % js_file)
        sys.exit(1)
    print('Opening %s...' % js_file)
    jsdev = open(js_file, 'rb')

    # Get the device name.
    buf = array.array('h', [0] * 64)
    ioctl(jsdev, 0x80006a13 + (0x10000 * len(buf)), buf)
    js_name = buf.tobytes().replace(b'\0', b'')
    print('Device name: [%s]' % js_name)

    if not js_name.startswith(b'Nintendo Wii Remote Balance Board'):
        print('%s does not look like a balance board' % fn)
        return None
    return jsdev

def save_data(weight, samples, out_file):
    data = []
    if os.path.exists(out_file):
        with open(out_file) as data_file:
            data = json.load(data_file)
    data.append([time.time(), weight, samples])
    with open(out_file, 'w') as data_file:
        json.dump(data, data_file)

def sample(jsdev, count=1, limit=0, dump=False, callback=None):
    samples = []
    weights = [0,0,0,0]
    sampling = True
    while sampling:
        evbuf = jsdev.read(8)
        if evbuf:
            timestamp, value, type, number = struct.unpack('IhBB', evbuf)
            if type & 0x01:
                button = number
                if value:
                    # press
                    break
                else:
                    # released
                    pass

            if type & 0x02:
                axis = number
                value = value + 32767.5
                weights[number] = value
                weight = sum(weights)
                if weight > limit:
                    samples.append(weight)
                    if dump:
                        print(weight/100.0)
                    if callback and hasattr(callback, '__call__'):
                        callback(weight/100.0)
            if len(samples) == count:
                sampling = False
    return samples

def disconnect(address):
    print('Disconecting')
    subprocess.check_output(["bt-device", "-d", address])

if __name__ == '__main__':
    arguments = docopt(__doc__, version='Wii Balance Board CLI v0.1')
    js_file = arguments['--jsdev']
    btaddress = arguments['--btaddress']
    count = int(arguments['--count'])
    limit = int(arguments['--limit'])
    dump = arguments['--out'] == None and not arguments['--summary']
    discard = int(arguments['--discard'])
    if arguments['--wait']:
        wait(js_file)
    jsdev = setup(js_file)
    samples = sample(jsdev, count, limit, dump)
    if len(samples) > 0:
        print('Result:')
        res = statistics.mean(samples[-discard:])
        print(res/100.0)
        if arguments['--out'] != None:
            save_data(res, samples, arguments['--out'])
    else:
        print('No samples')
    if btaddress:
        disconnect(btaddress)

