#!/usr/bin/env python3

# Released by asmw under the Unlicense (unlicense.org)
# Based on https://gist.github.com/rdb/8864666

"""Wii-Scale Client based on wbb.py

Usage: wbb-wii-scale.py [-b btaddress] [-j JSDEV]

-b BTADDRESS --btaddress=BTADDRESS  the Wii Balance Board to disconnect
-j JSDEV --jsdev=JSDEV              the joystick device to use [default: /dev/input/js0]
"""

from docopt import docopt
import wbb
import statistics

socket = None

if __name__ == '__main__':
    arguments = docopt(__doc__, version='Wii-Scale Dumper v0.1')
    btaddress = arguments['--btaddress']
    js_file = arguments['--jsdev']
    print("Connecting")
    wbb.wait(js_file)
    jsdev = wbb.setup(js_file)
    samples = wbb.sample(jsdev, 1500, 0)
    total = statistics.mean(samples[-500:])/100.0
    print(total)
